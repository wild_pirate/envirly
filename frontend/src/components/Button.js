const Button = (props) => (
  <button className="mt-20" {...props}>
    {props.children}
  </button>
);

export default Button;
