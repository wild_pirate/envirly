export const parseInputType = (type) => {
  switch (type) {
    case "NumericField":
      return "number";
    case "RegexValidatedField":
      return "text";
    default:
      return "text";
  }
};

const Input = ({ value, type, name, label, pattern, onChange = (e) => {} }) => (
  <div className="mt-20">
    <label>{label}</label>
    <input
      type={parseInputType(type)}
      name={name}
      label={label}
      pattern={pattern}
      value={value || ""}
      onChange={onChange}
    />
  </div>
);

export default Input;
