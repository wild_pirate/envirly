const MultiSelector = ({
  value,
  onChange = (value) => {},
  name,
  label,
  choices = [],
}) => (
  <div className="mt-20">
    <label>{label}</label>
    <div className="vertical-row">
      {choices.map((choice, idx) => {
        const key = choice + "+" + idx;
        const _value = value || [];
        const isChecked = !!_value.find((x) => x === choice);
        const handleOnChange = (e) => {
          const newValue = isChecked
            ? _value.filter((v) => v !== choice)
            : [..._value, choice];
          onChange(newValue);
        };
        return (
          <div key={key}>
            <label>
              <input
                name={name}
                label={choice}
                type="checkbox"
                checked={isChecked}
                onChange={handleOnChange}
              />
              {choice}
            </label>
          </div>
        );
      })}
    </div>
  </div>
);

export default MultiSelector;
