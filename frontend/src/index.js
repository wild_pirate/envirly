import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

import Button from "./components/Button";
import Input from "./Input";
import MultiSelector from "./MultiSelector";

// Assuming name key must be unique

const definition = JSON.parse(`{
  "url": "form_url",
  "fields": [
    {
      "type": "NumericField",
      "name": "number",
      "label": "Number field",
      "pattern": null,
      "choices": null
    },
    {
      "type": "RegexValidatedField",
      "name": "string",
      "label": "Only lowercase letters and underscores field",
      "pattern": "[a-z_]+",
      "choices": null
    },
    {
      "type": "MultiSelectField",
      "name": "selection",
      "label": "Multiselect field",
      "pattern": null,
      "choices": [
        "Choice A",
        "Choice B",
        "Choice C"
      ]
    }
  ]
}`);

function postData(url = "", data = {}) {
  const _data = JSON.stringify(data);
  try {
    const response = fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: _data,
    });
    return response.json();
  } catch (err) {
    const message = `Error sending data: ${_data}`;
    alert(message);
    throw new Error(message);
  }
}

// Implement the component here
function Form({ definition }) {
  const { url, fields } = definition;

  const initialFieldValues = Object.fromEntries(
    fields.map((field, idx) => [field.name, null])
  );

  const [inputValues, setInputValues] = React.useState(initialFieldValues);

  const handleInputValuesChange = (name, value) => {
    const change = Object.fromEntries([[name, value]]);
    setInputValues((prevState) => ({ ...prevState, ...change }));
  };

  const handleClearClick = () => setInputValues(initialFieldValues);
  const handleSubmitClick = () => postData(url, inputValues);

  return (
    <>
      <form onSubmit={handleSubmitClick}>
        {fields.map((field, idx) => {
          const { name, type } = field;
          const key = `${idx}+${name}`;
          const isMultiSelector = "MultiSelectField" === type;
          return isMultiSelector ? (
            <MultiSelector
              {...field}
              key={key}
              value={inputValues[name]}
              onChange={(value) => handleInputValuesChange(name, value)}
            />
          ) : (
            <Input
              {...field}
              key={key}
              value={inputValues[name]}
              onChange={(e) => handleInputValuesChange(name, e.target.value)}
            />
          );
        })}
        <input className="mt-20" type="submit" value="submit" />
      </form>
      <Button onClick={handleClearClick}>Clear</Button>
    </>
  );
}

const formContainer = document.querySelector("#form-container");
ReactDOM.createRoot(formContainer).render(<Form definition={definition} />);
