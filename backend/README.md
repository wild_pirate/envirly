# How to run:

copy env.exmaple file (and modify optionally)
```
cp env.example .env
```

build docker
```
docker-compose build
```

start docker
```
docker-compose up
```

the system will create superuser, you can change credentials in .env file:
usename: admin
password: Admin123!

# Using

After starting docker with
```
docker-compose up
```

1. open a web browser and go for http://localhost 
2. login with superuser credentials from .env file
3. use rest-framework UI to navigate: create new SomeModel instances, update them or delete

## Postman
When using postman please obtain auth-token from 
```
[POST] localhost/api-token-auth/

data = {
  "username": "admin",
  "password": "Admin123!"
}
or other defined in .env

```

The docker contains 3 services:
- db - postgres service
- web - django instance (served under port 8000)
- nginx - serving static files and django proxy (served under port 80)


# Testing

Run tests:
```
docker-compose run web pytest
```

