from types import new_class
import pytest
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from tests.factories.user import UserFactory
from tests.factories.some_model import SomeModelFactory


@pytest.mark.django_db
class TestUpdateSomeModelApi:

    def setup(self):
        self.user = UserFactory(is_superuser=True)
        self.token = Token.objects.create(user=self.user)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f"token {self.token}")

        self.instance = SomeModelFactory()
        self.url = f"/api/v1/some-model/{self.instance.id}/"

    def test_permissions(self):
        new_data = {
            "field_a": 22,
            "field_b": "test"
        }
        response = APIClient().put(self.url, new_data)
        assert response.status_code == 401

    def test_update_some_model(self):
        new_data = {
            "field_a": 22,
            "field_b": "test"
        }
        response = self.client.put(self.url, new_data)
        assert response.status_code == 200

    def test_update_some_model_big_integers_validation(self):
        new_data = {
            "field_a": 1234567891011,
            "field_b": "2"
        }
        response = self.client.put(self.url, new_data)
        assert response.status_code == 400
