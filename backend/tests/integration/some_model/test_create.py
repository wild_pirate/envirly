import pytest
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from tests.factories.user import UserFactory


@pytest.mark.django_db
class TestCreateSomeModelApi:

    def setup(self):
        self.user = UserFactory(is_superuser=True)
        self.token = Token.objects.create(user=self.user)

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f"token {self.token}")

    def test_permissions(self):
        response = APIClient().post("/api/v1/some-model/", {})
        assert response.status_code == 401

    def test_create_some_model(self):
        data = {
            "field_a": 1,
            "field_b": "2"
        }
        response = self.client.post("/api/v1/some-model/", data)
        assert response.status_code == 201

    def test_some_model_big_integers_validation(self):
        data = {
            "field_a": 1234567891011,
            "field_b": "2"
        }
        response = self.client.post("/api/v1/some-model/", data)
        assert response.status_code == 400
