import factory
from faker import Faker

from api.models import SomeModel


fake = Faker()


class SomeModelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SomeModel

    field_a = fake.random_int(1, 100)
    field_b = fake.name()[:9]
