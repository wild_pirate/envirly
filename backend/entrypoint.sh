#!/bin/bash

# Collect static
python manage.py collectstatic --noinput 

# Migrate
python manage.py migrate 

# Run the Django management command to create the superuser
python manage.py createsuperuser --noinput \
    --username=$SUPERUSER_USERNAME --email=$SUPERUSER_EMAIL

# Set superuser password
echo "from django.contrib.auth import get_user_model; User = get_user_model(); \
      u = User.objects.get(username='$SUPERUSER_USERNAME'); u.set_password('$SUPERUSER_PASSWORD'); u.save()" | \
      python manage.py shell

echo "$SUPERUSER_USERNAME Superuser created!"

# Run dev server
python manage.py runserver 0.0.0.0:8000
