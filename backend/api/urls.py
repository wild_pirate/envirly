from django.urls import path, include
from rest_framework.routers import DefaultRouter

from api import views
from api.receivers import *

router = DefaultRouter()
router.register("some-model", views.SomeModelViewSet)


urlpatterns = [
    path("", include(router.urls)),
]
