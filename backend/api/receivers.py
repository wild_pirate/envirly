from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone

from api.models import SomeModel


@receiver(pre_save, sender=SomeModel)
def some_model_pre_save(sender, instance, **kwargs):
    """
    Receiver that updates last_modification timestamp and 
    increases number_of_udates for existing instances
    """
    instance.last_modification = int(timezone.now().timestamp())
    old_instance = SomeModel.objects.filter(id=instance.id).first()
    if old_instance:
        instance.number_of_updates = old_instance.number_of_updates + 1
