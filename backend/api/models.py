from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone


def timestamp_now():
    now = timezone.now()
    return now.timestamp()


class SomeModel(models.Model):
    """
    This is task model,
    yet the validation in the code was invalid and ignored by django.
    Replaced max_length in field_a with proper validator.
    """

    field_a = models.IntegerField(
        validators=[
            MaxValueValidator(9999999999),
            MinValueValidator(-9999999999)
        ]
    )
    field_b = models.CharField(max_length=10)
    number_of_updates = models.IntegerField(default=0)
    last_modification = models.IntegerField(default=timestamp_now)
