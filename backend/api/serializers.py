from rest_framework.serializers import ModelSerializer

from api.models import SomeModel


class SomeModelSerializer(ModelSerializer):

    class Meta:
        model = SomeModel
        fields = "__all__"
        read_only_fields = ("last_modification", "number_of_updates", )
