from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from api.models import SomeModel
from api import serializers


class SomeModelViewSet(ModelViewSet):
    queryset = SomeModel.objects.all()
    serializer_class = serializers.SomeModelSerializer
