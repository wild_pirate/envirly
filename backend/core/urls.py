from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include
from django.http import HttpResponseNotFound
from rest_framework.authtoken import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
    path("api-token-auth/", views.obtain_auth_token),
    path("api/v1/", include("api.urls"))
]


def handle404(request, *args, **kwargs):
    if request.method == "GET":
        return redirect("/api/v1/")
    return HttpResponseNotFound()


handler404 = handle404
