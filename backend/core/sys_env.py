import os

DB_NAME = os.environ.get("DB_NAME", "not-set")
DB_USER = os.environ.get("DB_USER", "not-set")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "not-set")
DB_HOST = os.environ.get("DB_HOST", "not-set")
DB_PORT = os.environ.get("DB_PORT", "not-set")

_DEBUG = os.environ.get("DEBUG", "not-set")
DEBUG = _DEBUG.lower() in ["1", "true"]

SECRET_KEY = os.environ.get("SECRET_KEY", "not-set")
ALLOWED_HOSTS = os.environ.get("ALLOWED_HOSTS", "").split(",")
